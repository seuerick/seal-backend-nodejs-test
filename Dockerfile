FROM node:alpine

WORKDIR /root

COPY package.json .

RUN npm install --quiet

RUN npm install nodemon -g --quiet

COPY . . 

EXPOSE 8000

CMD nodemon -L --watch . index.js
