const mongoose = require('mongoose');

const EquipamentoSchema = mongoose.Schema({
	
	sku: {
		type: String,
		required: true
	},
	descricao: {
		type: String,
		require: true
	},
	categoria: {
		type: String,
		require: true
	},
	preco: {
		type: String,
		require: true
	},
	status: {
		type: String,
		require: true,
		select: false
	}
});

module.exports = mongoose.model('equipamento', EquipamentoSchema);