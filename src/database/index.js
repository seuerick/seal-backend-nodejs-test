const mongoose = require('mongoose');

mongoose.connect('mongodb://db:27017/seal-erick', { useMongoClient: true });

mongoose.Promise = global.Promise;

module.exports = mongoose;