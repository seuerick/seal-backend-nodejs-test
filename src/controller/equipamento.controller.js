const express = require('express');

const Equipamento = requeri('../model/Equipamento');

const router = express.Router();

router.get('/', (req, res) => {
  Equipamento.find().then(equipamentos => {
      res.json(equipamentos);
    })
    .catch(error => res.status(500).json(error));
});

router.get('/:id', (req, res) => {
  Equipamento.findOne({ _id: req.params.id }).then(equipamento => {
      res.json(equipamento);
    })
    .catch(error => res.status(500).json(error));
});

router.post('/', (req, res) => {
	try {
		const equipamento = await Equipamento.create(req.body);
		return res.send(equipamento);
	} catch(err) {
		return res.status(400).send({ msg: 'Falha ao incluir Equipamento' })
	}
});

router.put('/:id', (req, res) => {

  Equipamento.findOneAndUpdate({ _id: req.params.id }, req.body).then(equipamento => {
      res.json(equipamento);
    })
    .catch(error => res.status(500).json(error));
});

router.delete('/:id', (req, res) => {
  Equipamento.findOneAndDelete({ _id: req.params.id }).then(equipamento => {
      res.json(equipamento);
    })
    .catch(error => res.status(500).json(error));
});

module.exports = app => app.use('/equipamento')